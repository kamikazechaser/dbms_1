<?php
  $servername = "localhost";
  $username = "newMySqlUsername";
  $password = "mysqlNewUsernamePassword";
  $dbname = "generator";

  # connect to the database
  $conn = new mysqli($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }
  //query for getting max classes per subj
  $totz = "select name, count(*) as TOTAL from attendances, subjects where subjects.id = attendances.subject and attendances.deleted_at is NULL group by subject;";
$total_classes = Array();
  $result = $conn->query($totz);
 
  if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
            $total_stru = Array();
            array_push($total_stru,$row["name"]);
            array_push($total_stru,$row["TOTAL"]);
            array_push($total_classes,$total_stru);
        }
  } else {
      echo "0 results";
  }

//comparator fn for sorting $doc
  function cmp1($a, $b)
  {
      if ($a[1] == $b[1]) {
          if($a[0]==$b[0])
            return 0;
          return ($a[0] > $b[0])?1:-1;
      }
      if ($a[1] > $b[1]){
        //   return ($a[2] > $b[2])?1:-1;
        return 1;
      }
      return -1;
  }
  
  //getting stduents list
  $studs = Array();
  # execute a query and output its results
  $sql = "SELECT * FROM students";

  $result = $conn->query($sql);
 
  if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
          array_push($studs,$row["roll"]);
        }
  } else {
      echo "0 results";
  }

// query for getting bunks per subject per stud
  $doc = Array();
//   echo var_dump($studs);
  foreach ($studs as $stud){ 
    //code to be executed;
    $qr = "select ST.name,floor( SUM((length(absentees)-length(replace(absentees ,$stud,'')))/9) ) as COUNT FROM attendances as AT,subjects as ST where AT.subject = ST.id and AT.deleted_at is NULL group by subject;";
    $result = $conn->query($qr);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $struc = Array();
            array_push($struc, $stud);
            array_push($struc, $row["name"]);
            array_push($struc, $row["COUNT"]);
            array_push($doc,$struc);
            // if($row["COUNT"] == 1){
            //     echo $stud ." has not attended ". $row["COUNT"]. " a class in " . $row["name"]."\n";
            // }
            // if($row["COUNT"] > 1)
            // echo $stud ." has not attended ". $row["COUNT"]. " classes in " . $row["name"]."\n";
          }

    } else {
        echo "0 results";
    }  
}
    usort($doc,"cmp1");
    $tot_class = 1;
    echo "<table class=\"table table-bordered text-center\" 
>
      <tr 
>
        <th 
>Roll Number</th>
        <th 
>Subject</th>
        <th 
>Classes not attended</th>
        <th 
>Attendance Percentage</th>
      </tr>";
    foreach ($doc as $do){

        foreach($total_classes as $t){
            if($t[0]==$do[1]){
                $tot_class = $t[1];
            }
        }
        array_push($do, ceil(100*(($tot_class - $do[2])/$tot_class)));
        // var_dump($do);
        if($do[2]!=0)
        //   foreach ($do as $row){
            if($do[3] >=75){       
          echo "<tr class=\"success\"><td 
>$do[0]</td>
                <td 
>$do[1]</td>
                <td 
>$do[2]</td>
                <td 
>$do[3]</td>
            </tr>";
            }
            else{
          echo "<tr class=\"danger\"><td 
          >$do[0]</td>
                          <td 
          >$do[1]</td>
                          <td 
          >$do[2]</td>
                          <td 
          >$do[3]</td>
                      </tr>";
            }
            // }  
        }
        echo "</table>";    
        
        
  $conn->close();

?>