<?php

namespace App\Providers;
use App\Models\Subject;
use App\Models\Student;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['attendances.fields'], function ($view) {
            $subjectItems = Subject::pluck('name','id')->toArray();
            $view->with('subjectItems', $subjectItems);
        });
        View::composer(['attendances.fields'], function ($view) {
            $studentItems = Student::pluck('name','id')->toArray();
            $view->with('studentItems', $studentItems);
        });
        View::composer(['attendances.fields'], function ($view) {
            $studentItems = Student::pluck('name','id')->toArray();
            $view->with('studentItems', $studentItems);
        });
        View::composer(['attendances.fields'], function ($view) {
            $studentItems = Student::pluck('name','id')->toArray();
            $view->with('studentItems', $studentItems);
        });
        //
    }
}