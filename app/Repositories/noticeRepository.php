<?php

namespace App\Repositories;

use App\Models\notice;
use App\Repositories\BaseRepository;

/**
 * Class noticeRepository
 * @package App\Repositories
 * @version November 7, 2019, 11:59 am UTC
*/

class noticeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'body'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return notice::class;
    }
}
