<?php

namespace App\Repositories;

use App\Models\subject;
use App\Repositories\BaseRepository;

/**
 * Class subjectRepository
 * @package App\Repositories
 * @version November 7, 2019, 12:41 pm UTC
*/

class subjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return subject::class;
    }
}
