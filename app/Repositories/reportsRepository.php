<?php

namespace App\Repositories;

use App\Models\reports;
use App\Repositories\BaseRepository;

/**
 * Class reportsRepository
 * @package App\Repositories
 * @version November 7, 2019, 3:57 pm UTC
*/

class reportsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return reports::class;
    }
}
