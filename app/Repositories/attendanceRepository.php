<?php

namespace App\Repositories;

use App\Models\attendance;
use App\Repositories\BaseRepository;

/**
 * Class attendanceRepository
 * @package App\Repositories
 * @version November 7, 2019, 12:44 pm UTC
*/

class attendanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subject',
        'absentees'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return attendance::class;
    }
}
