<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatestudentAPIRequest;
use App\Http\Requests\API\UpdatestudentAPIRequest;
use App\Models\student;
use App\Repositories\studentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class studentController
 * @package App\Http\Controllers\API
 */

class studentAPIController extends AppBaseController
{
    /** @var  studentRepository */
    private $studentRepository;

    public function __construct(studentRepository $studentRepo)
    {
        $this->studentRepository = $studentRepo;
    }

    /**
     * Display a listing of the student.
     * GET|HEAD /students
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $students = $this->studentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($students->toArray(), 'Students retrieved successfully');
    }

    /**
     * Store a newly created student in storage.
     * POST /students
     *
     * @param CreatestudentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatestudentAPIRequest $request)
    {
        $input = $request->all();

        $student = $this->studentRepository->create($input);

        return $this->sendResponse($student->toArray(), 'Student saved successfully');
    }

    /**
     * Display the specified student.
     * GET|HEAD /students/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var student $student */
        $student = $this->studentRepository->find($id);

        if (empty($student)) {
            return $this->sendError('Student not found');
        }

        return $this->sendResponse($student->toArray(), 'Student retrieved successfully');
    }

    /**
     * Update the specified student in storage.
     * PUT/PATCH /students/{id}
     *
     * @param int $id
     * @param UpdatestudentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatestudentAPIRequest $request)
    {
        $input = $request->all();

        /** @var student $student */
        $student = $this->studentRepository->find($id);

        if (empty($student)) {
            return $this->sendError('Student not found');
        }

        $student = $this->studentRepository->update($input, $id);

        return $this->sendResponse($student->toArray(), 'student updated successfully');
    }

    /**
     * Remove the specified student from storage.
     * DELETE /students/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var student $student */
        $student = $this->studentRepository->find($id);

        if (empty($student)) {
            return $this->sendError('Student not found');
        }

        $student->delete();

        return $this->sendResponse($id, 'Student deleted successfully');
    }
}
