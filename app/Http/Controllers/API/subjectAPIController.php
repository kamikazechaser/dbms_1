<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatesubjectAPIRequest;
use App\Http\Requests\API\UpdatesubjectAPIRequest;
use App\Models\subject;
use App\Repositories\subjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class subjectController
 * @package App\Http\Controllers\API
 */

class subjectAPIController extends AppBaseController
{
    /** @var  subjectRepository */
    private $subjectRepository;

    public function __construct(subjectRepository $subjectRepo)
    {
        $this->subjectRepository = $subjectRepo;
    }

    /**
     * Display a listing of the subject.
     * GET|HEAD /subjects
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $subjects = $this->subjectRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($subjects->toArray(), 'Subjects retrieved successfully');
    }

    /**
     * Store a newly created subject in storage.
     * POST /subjects
     *
     * @param CreatesubjectAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatesubjectAPIRequest $request)
    {
        $input = $request->all();

        $subject = $this->subjectRepository->create($input);

        return $this->sendResponse($subject->toArray(), 'Subject saved successfully');
    }

    /**
     * Display the specified subject.
     * GET|HEAD /subjects/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var subject $subject */
        $subject = $this->subjectRepository->find($id);

        if (empty($subject)) {
            return $this->sendError('Subject not found');
        }

        return $this->sendResponse($subject->toArray(), 'Subject retrieved successfully');
    }

    /**
     * Update the specified subject in storage.
     * PUT/PATCH /subjects/{id}
     *
     * @param int $id
     * @param UpdatesubjectAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesubjectAPIRequest $request)
    {
        $input = $request->all();

        /** @var subject $subject */
        $subject = $this->subjectRepository->find($id);

        if (empty($subject)) {
            return $this->sendError('Subject not found');
        }

        $subject = $this->subjectRepository->update($input, $id);

        return $this->sendResponse($subject->toArray(), 'subject updated successfully');
    }

    /**
     * Remove the specified subject from storage.
     * DELETE /subjects/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var subject $subject */
        $subject = $this->subjectRepository->find($id);

        if (empty($subject)) {
            return $this->sendError('Subject not found');
        }

        $subject->delete();

        return $this->sendResponse($id, 'Subject deleted successfully');
    }
}
