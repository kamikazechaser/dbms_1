<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatenoticeAPIRequest;
use App\Http\Requests\API\UpdatenoticeAPIRequest;
use App\Models\notice;
use App\Repositories\noticeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class noticeController
 * @package App\Http\Controllers\API
 */

class noticeAPIController extends AppBaseController
{
    /** @var  noticeRepository */
    private $noticeRepository;

    public function __construct(noticeRepository $noticeRepo)
    {
        $this->noticeRepository = $noticeRepo;
    }

    /**
     * Display a listing of the notice.
     * GET|HEAD /notices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $notices = $this->noticeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($notices->toArray(), 'Notices retrieved successfully');
    }

    /**
     * Store a newly created notice in storage.
     * POST /notices
     *
     * @param CreatenoticeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatenoticeAPIRequest $request)
    {
        $input = $request->all();

        $notice = $this->noticeRepository->create($input);

        return $this->sendResponse($notice->toArray(), 'Notice saved successfully');
    }

    /**
     * Display the specified notice.
     * GET|HEAD /notices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var notice $notice */
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            return $this->sendError('Notice not found');
        }

        return $this->sendResponse($notice->toArray(), 'Notice retrieved successfully');
    }

    /**
     * Update the specified notice in storage.
     * PUT/PATCH /notices/{id}
     *
     * @param int $id
     * @param UpdatenoticeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenoticeAPIRequest $request)
    {
        $input = $request->all();

        /** @var notice $notice */
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            return $this->sendError('Notice not found');
        }

        $notice = $this->noticeRepository->update($input, $id);

        return $this->sendResponse($notice->toArray(), 'notice updated successfully');
    }

    /**
     * Remove the specified notice from storage.
     * DELETE /notices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var notice $notice */
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            return $this->sendError('Notice not found');
        }

        $notice->delete();

        return $this->sendResponse($id, 'Notice deleted successfully');
    }
}
