<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatenoticeRequest;
use App\Http\Requests\UpdatenoticeRequest;
use App\Repositories\noticeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class noticeController extends AppBaseController
{
    /** @var  noticeRepository */
    private $noticeRepository;

    public function __construct(noticeRepository $noticeRepo)
    {
        $this->noticeRepository = $noticeRepo;
    }

    /**
     * Display a listing of the notice.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $notices = $this->noticeRepository->all();

        return view('notices.index')
            ->with('notices', $notices);
    }

    /**
     * Show the form for creating a new notice.
     *
     * @return Response
     */
    public function create()
    {
        return view('notices.create');
    }

    /**
     * Store a newly created notice in storage.
     *
     * @param CreatenoticeRequest $request
     *
     * @return Response
     */
    public function store(CreatenoticeRequest $request)
    {
        $input = $request->all();

        $notice = $this->noticeRepository->create($input);

        Flash::success('Notice saved successfully.');

        return redirect(route('notices.index'));
    }

    /**
     * Display the specified notice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            Flash::error('Notice not found');

            return redirect(route('notices.index'));
        }

        return view('notices.show')->with('notice', $notice);
    }

    /**
     * Show the form for editing the specified notice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            Flash::error('Notice not found');

            return redirect(route('notices.index'));
        }

        return view('notices.edit')->with('notice', $notice);
    }

    /**
     * Update the specified notice in storage.
     *
     * @param int $id
     * @param UpdatenoticeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenoticeRequest $request)
    {
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            Flash::error('Notice not found');

            return redirect(route('notices.index'));
        }

        $notice = $this->noticeRepository->update($request->all(), $id);

        Flash::success('Notice updated successfully.');

        return redirect(route('notices.index'));
    }

    /**
     * Remove the specified notice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $notice = $this->noticeRepository->find($id);

        if (empty($notice)) {
            Flash::error('Notice not found');

            return redirect(route('notices.index'));
        }

        $this->noticeRepository->delete($id);

        Flash::success('Notice deleted successfully.');

        return redirect(route('notices.index'));
    }
}
