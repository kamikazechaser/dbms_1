<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class attendance
 * @package App\Models
 * @version November 7, 2019, 12:44 pm UTC
 *
 * @property integer subject
 * @property string absentees
 */
class attendance extends Model
{
    use SoftDeletes;

    public $table = 'attendances';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'subject',
        'absentees'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subject' => 'integer',
        'absentees' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subject' => 'required',
        'absentees' => 'required'
    ];

    
}
