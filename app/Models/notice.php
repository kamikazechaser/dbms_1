<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class notice
 * @package App\Models
 * @version November 7, 2019, 11:59 am UTC
 *
 * @property string title
 * @property string body
 */
class notice extends Model
{
    use SoftDeletes;

    public $table = 'notices';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'body'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'body' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'body' => 'required'
    ];

    
}
