<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\attendance;
use Faker\Generator as Faker;

$factory->define(attendance::class, function (Faker $faker) {

    return [
        'subject' => $faker->randomDigitNotNull,
        'absentees' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
