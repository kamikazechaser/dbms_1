<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\class attendance;
use Faker\Generator as Faker;

$factory->define(class attendance::class, function (Faker $faker) {

    return [
        'chumma' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
