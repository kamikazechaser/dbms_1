<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\notice;

class noticeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_notice()
    {
        $notice = factory(notice::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/notices', $notice
        );

        $this->assertApiResponse($notice);
    }

    /**
     * @test
     */
    public function test_read_notice()
    {
        $notice = factory(notice::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/notices/'.$notice->id
        );

        $this->assertApiResponse($notice->toArray());
    }

    /**
     * @test
     */
    public function test_update_notice()
    {
        $notice = factory(notice::class)->create();
        $editednotice = factory(notice::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/notices/'.$notice->id,
            $editednotice
        );

        $this->assertApiResponse($editednotice);
    }

    /**
     * @test
     */
    public function test_delete_notice()
    {
        $notice = factory(notice::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/notices/'.$notice->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/notices/'.$notice->id
        );

        $this->response->assertStatus(404);
    }
}
