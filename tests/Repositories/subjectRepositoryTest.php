<?php namespace Tests\Repositories;

use App\Models\subject;
use App\Repositories\subjectRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class subjectRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var subjectRepository
     */
    protected $subjectRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->subjectRepo = \App::make(subjectRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_subject()
    {
        $subject = factory(subject::class)->make()->toArray();

        $createdsubject = $this->subjectRepo->create($subject);

        $createdsubject = $createdsubject->toArray();
        $this->assertArrayHasKey('id', $createdsubject);
        $this->assertNotNull($createdsubject['id'], 'Created subject must have id specified');
        $this->assertNotNull(subject::find($createdsubject['id']), 'subject with given id must be in DB');
        $this->assertModelData($subject, $createdsubject);
    }

    /**
     * @test read
     */
    public function test_read_subject()
    {
        $subject = factory(subject::class)->create();

        $dbsubject = $this->subjectRepo->find($subject->id);

        $dbsubject = $dbsubject->toArray();
        $this->assertModelData($subject->toArray(), $dbsubject);
    }

    /**
     * @test update
     */
    public function test_update_subject()
    {
        $subject = factory(subject::class)->create();
        $fakesubject = factory(subject::class)->make()->toArray();

        $updatedsubject = $this->subjectRepo->update($fakesubject, $subject->id);

        $this->assertModelData($fakesubject, $updatedsubject->toArray());
        $dbsubject = $this->subjectRepo->find($subject->id);
        $this->assertModelData($fakesubject, $dbsubject->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_subject()
    {
        $subject = factory(subject::class)->create();

        $resp = $this->subjectRepo->delete($subject->id);

        $this->assertTrue($resp);
        $this->assertNull(subject::find($subject->id), 'subject should not exist in DB');
    }
}
