<?php namespace Tests\Repositories;

use App\Models\notice;
use App\Repositories\noticeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class noticeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var noticeRepository
     */
    protected $noticeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->noticeRepo = \App::make(noticeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_notice()
    {
        $notice = factory(notice::class)->make()->toArray();

        $creatednotice = $this->noticeRepo->create($notice);

        $creatednotice = $creatednotice->toArray();
        $this->assertArrayHasKey('id', $creatednotice);
        $this->assertNotNull($creatednotice['id'], 'Created notice must have id specified');
        $this->assertNotNull(notice::find($creatednotice['id']), 'notice with given id must be in DB');
        $this->assertModelData($notice, $creatednotice);
    }

    /**
     * @test read
     */
    public function test_read_notice()
    {
        $notice = factory(notice::class)->create();

        $dbnotice = $this->noticeRepo->find($notice->id);

        $dbnotice = $dbnotice->toArray();
        $this->assertModelData($notice->toArray(), $dbnotice);
    }

    /**
     * @test update
     */
    public function test_update_notice()
    {
        $notice = factory(notice::class)->create();
        $fakenotice = factory(notice::class)->make()->toArray();

        $updatednotice = $this->noticeRepo->update($fakenotice, $notice->id);

        $this->assertModelData($fakenotice, $updatednotice->toArray());
        $dbnotice = $this->noticeRepo->find($notice->id);
        $this->assertModelData($fakenotice, $dbnotice->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_notice()
    {
        $notice = factory(notice::class)->create();

        $resp = $this->noticeRepo->delete($notice->id);

        $this->assertTrue($resp);
        $this->assertNull(notice::find($notice->id), 'notice should not exist in DB');
    }
}
