<?php namespace Tests\Repositories;

use App\Models\student;
use App\Repositories\studentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class studentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var studentRepository
     */
    protected $studentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->studentRepo = \App::make(studentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_student()
    {
        $student = factory(student::class)->make()->toArray();

        $createdstudent = $this->studentRepo->create($student);

        $createdstudent = $createdstudent->toArray();
        $this->assertArrayHasKey('id', $createdstudent);
        $this->assertNotNull($createdstudent['id'], 'Created student must have id specified');
        $this->assertNotNull(student::find($createdstudent['id']), 'student with given id must be in DB');
        $this->assertModelData($student, $createdstudent);
    }

    /**
     * @test read
     */
    public function test_read_student()
    {
        $student = factory(student::class)->create();

        $dbstudent = $this->studentRepo->find($student->id);

        $dbstudent = $dbstudent->toArray();
        $this->assertModelData($student->toArray(), $dbstudent);
    }

    /**
     * @test update
     */
    public function test_update_student()
    {
        $student = factory(student::class)->create();
        $fakestudent = factory(student::class)->make()->toArray();

        $updatedstudent = $this->studentRepo->update($fakestudent, $student->id);

        $this->assertModelData($fakestudent, $updatedstudent->toArray());
        $dbstudent = $this->studentRepo->find($student->id);
        $this->assertModelData($fakestudent, $dbstudent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_student()
    {
        $student = factory(student::class)->create();

        $resp = $this->studentRepo->delete($student->id);

        $this->assertTrue($resp);
        $this->assertNull(student::find($student->id), 'student should not exist in DB');
    }
}
