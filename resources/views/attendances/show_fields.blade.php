<!-- Subject Field -->
<div class="form-group">
    {!! Form::label('subject', 'Subject:') !!}
    <p>{!! $attendance->subject !!}</p>
</div>

<!-- Absentees Field -->
<div class="form-group">
    {!! Form::label('absentees', 'Absentees:') !!}
    <p>{!! $attendance->absentees !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $attendance->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $attendance->updated_at !!}</p>
</div>

