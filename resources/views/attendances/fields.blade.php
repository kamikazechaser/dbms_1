<!-- Subject Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject', 'Subject:') !!}
    {!! Form::select('subject', $subjectItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Absentees Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('absentees', 'Absentees:') !!}
    {!! Form::textarea('absentees', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('attendances.index') !!}" class="btn btn-default">Cancel</a>
</div>
