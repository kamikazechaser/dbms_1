<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $student->name !!}</p>
</div>

<!-- Roll Field -->
<div class="form-group">
    {!! Form::label('roll', 'Roll:') !!}
    <p>{!! $student->roll !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $student->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $student->updated_at !!}</p>
</div>

